# Cosa para compactar filas

(se puede extender más o menos fácilmente para hacer otras cosas)

## Modo de uso

Hay que tener rust para compilarlo (mas no para correrlo). Se puede descargar
de [rustup](https://rustup.rs).

Una vez clonado el repo solamente hay que ejecutar dentro de la carpeta:

    cargo build --release

Lo cual va a generar un archivo `target/release/estabase` que se puede usar para
resolver el problema en cuestión. Si se coloca en el PATH entonces simplemente
se puede usar por su nombre, si no su ruta relativa bastará.

Por ejemplo suponiendo que tienes un archivo csv en `/home/x/datos.csv` y que el
binario de este programa está en `/home/x/bin/estabase` entonces se puede
ejecutar escribiendo:

    /home/x/bin/estabase /home/x/datos.csv

Al correrlo se verá como la salida se imprime en la consola, se puede guardar en
un archivo con:

    /home/x/bin/estabase /home/x/datos.csv > salida.csv
