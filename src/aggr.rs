use csvsc::prelude::*;

fn is_empty(s: &str) -> bool {
    s.len() == 0 || s.trim() == "n/a"
}

#[derive(Debug)]
pub struct LastNotEmpty {
    column: String,
    value: String,
}

impl LastNotEmpty {
    pub fn new(column: &str) -> LastNotEmpty {
        LastNotEmpty {
            column: column.into(),
            value: String::new(),
        }
    }
}

impl Aggregate for LastNotEmpty {
    fn update(&mut self, headers: &Headers, row: &Row) -> csvsc::error::Result<()> {
        let maybe_new_value = headers.get_field(row, &self.column).unwrap();

        if !is_empty(maybe_new_value) {
            self.value = maybe_new_value.into();
        }

        Ok(())
    }

    fn colname(&self) -> &str {
        &self.column
    }

    fn value(&self) -> String {
        format!("{}", self.value)
    }
}
