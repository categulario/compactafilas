use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Seek, SeekFrom};

use csvsc::prelude::*;
use csv::ReaderBuilder;

mod aggr;

use aggr::LastNotEmpty;

fn main() -> io::Result<()> {
    // Lee los argumentos de línea de comandos
    let args: Vec<_> = env::args().collect();

    // Extrae el primero como nombre de archivo de entrada
    let filename = args.get(1).expect("Need a file argument");

    // Abre el archivo indicado
    let mut file = File::open(filename)?;

    // Usa un bufer para leer su primera línea
    let mut buffered_file = BufReader::new(&file);
    let mut buf = String::new();

    // lee la primera linea, son las cabeceras
    buffered_file.read_line(&mut buf)?;

    // resetea el archivo al inicio, para que no afecte a csvsc
    file.seek(SeekFrom::Start(0))?;

    // La cadena de transformación, inicia con el archivo que ya abrimos
    let mut chain = InputStreamBuilder::from_readers(vec![
            ReaderBuilder::new()
                .from_reader(file).into()
        ])
        .build().unwrap()

        // agrupa por ID
        .group(["ID"], |rows| {
            // Reduce las columnas usando como valor para cada columna el último
            // valor que no sea n/a
            rows.reduce(
                buf
                    .trim()
                    .split(',')
                    .map(|s| Reducer::custom(LastNotEmpty::new(s)))
                    .collect()
            ).unwrap()
        })

        // Manda el resultado a la salida estándar
        .flush(Target::stdout()).unwrap()

        .into_iter();

    // And finally consume the stream, reporting any errors to stderr.
    while let Some(item) = chain.next() {
        if let Err(e) = item {
            eprintln!("{}", e);
        }
    }

    Ok(())
}
